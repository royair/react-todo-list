import {
  ADD_TODO,
  TOGGLE_TODO,
  REMOVE_TODO,
  CLEAR_COMPLETED
} from "../actionTypes";

const initialState = {
  tasks: [
    { id: 1, completed: false, title: 'read newspaper' },
    { id: 2, completed: false, title: 'take the dog out' },
    { id: 3, completed: false, title: 'clean the house' }
  ]
};


export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_TODO: {
      return {
        tasks: [
          ...state.tasks,
          action.newTask
        ]
      }
    }

    case TOGGLE_TODO: {
      action.task.completed = !action.task.completed;

      const newTasks = state.tasks.map(task => task.id === action.task.id ? action.task : task);
      return {
        tasks: [
          ...newTasks,
        ]
      }
    }

    case REMOVE_TODO: {
      const id = action.task.id;
      const filteredTasks = state.tasks.filter(task => task.id !== id);

      return {
        tasks: [
          ...filteredTasks,
        ]
      }
    }

    case CLEAR_COMPLETED: {
      const filteredTasks = state.tasks.filter(task => !task.completed)

      return {
        tasks: [
          ...filteredTasks,
        ]
      }
    }

    default:
      return state;
  }
}