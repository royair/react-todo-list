import React, { Component } from 'react';
import { connect } from 'react-redux'
import './TodoApp.scss';
import TodoItem from './TodoItem';
import guid from '../utils/guid';

class TodoApp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tasks: [],
      newTitle: ''
    }
  };

  onChangeNewTitle = (e) => {
    this.setState({ newTitle: e.target.value });
  };

  onAdd = () => {
    if (this.state.newTitle.length === 0) return;

    const newTask = {
      id: guid(),
      completed: false,
      title: this.state.newTitle
    };

    this.props.add(newTask);
    this.setState({ newTitle: '' })
  };

  onRemove = (task) => {
    this.props.remove(task);
    this.setState({ newTitle: '' });
  };

  onToggle = (task) => {
    this.props.toggle(task);
  };

  onClearCompleted = () => {
    this.props.clearCompleted();
  };

  count = () => {
    return this.props.tasks.filter(task => !task.completed).length;
  };

  isShowClearCompleted = () => {
    return this.props.tasks.reduce((prev, current,) => current.completed ? true : prev, false);
  };

  render() {
    let items = this.props.tasks.map(task => (
      <TodoItem key={task.id}
                task={task}
                onToggle={this.onToggle}
                onRemove={this.onRemove}/>
    ));
    return (
      <>
        <h1>todos</h1>
        <section className="todo-app">

          <section className="header">
            <div>
              <input type="text"
                     placeholder="What needs to be done?"
                     className={'new-todo'}
                     value={this.state.newTitle}
                     onChange={this.onChangeNewTitle}
                     onKeyPress={e => e.key === 'Enter' && this.onAdd()}/>
            </div>
          </section>
          <section className="main">
            <ul className="todo-list">
              {items}
            </ul>
          </section>
          <section className="footer">
            <div className="todo-count">
              {this.count()} items left
            </div>
            {/*<ul className="filters">
              <li className="selected"><a href="#/">All</a></li>
              <li><a href="#/"> Active</a></li>
              <li><a href="#/">Completed</a></li>
            </ul>*/}
            {this.isShowClearCompleted() &&
            <button className="clear-completed"
                    onClick={this.onClearCompleted}>Clear completed
            </button>
            }
          </section>
        </section>
      </>
    )
  }
};

const mapStateToProps = state => ({
  tasks: state.todos.tasks
});

const mapDispatchToProps = dispatch => ({
  add: (newTask) => dispatch({ type: 'ADD_TODO', newTask }),
  remove: (task) => dispatch({ type: 'REMOVE_TODO', task }),
  toggle: (task) => dispatch({ type: 'TOGGLE_TODO', task }),
  clearCompleted: () => dispatch({ type: 'CLEAR_COMPLETED' })
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoApp);
