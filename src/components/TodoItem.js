import React, { Component } from 'react';
import './TodoItem.scss';

class TodoItem extends Component {
  render() {
    const { task, onToggle, onRemove } = this.props;

    return (
      <>
        <li key={task.id}
            className={task.completed ? 'completed' : undefined}>
          <input type="checkbox"
                 defaultChecked={task.completed}
                 className="toggle"
                 onClick={() => onToggle(task)}/>
          <label>{task.title}</label>
          <button className="destroy"
                  onClick={() => onRemove(task)}>
            <svg viewBox="-20 -20 80 80">
              <path className="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30"/>
            </svg>
          </button>
        </li>
      </>
    )
  }
};

export default TodoItem;
