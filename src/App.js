import React, { Component } from 'react';

import { Provider } from "react-redux";
import store from "./store/store";


import './css/reset.css';
import './App.scss';

import TodoApp from './components/TodoApp.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Provider store={store}>
          <TodoApp/>
        </Provider>
      </div>
    );
  }
}

export default App;
